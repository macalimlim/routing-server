# routing-server

> A server that routes requests to the echo server(s) using a round-robin strategy

## Setup

You need to have [leiningen](https://leiningen.org/) and [JDK](https://jdk.java.net/) installed

Clone this project
```sh
$ git clone https://gitlab.com/macalimlim/routing-server
```

Configure the project for local development
```sh
$ lein duct setup
```

## Usage

To build the application

```shell
$ lein clean
$ lein uberjar
```

To run the application

```shell
$ java -jar ./target/routing-server-0.1.0-SNAPSHOT-standalone.jar # run the application with a default port of 9000, timeout of 5 and retries of 10
$ ROUTING_SERVER_PORT=9001 ROUTING_SERVER_TIMEOUT=10 ROUTING_SERVER_RETRIES=15 java -jar ./target/routing-server-0.1.0-SNAPSHOT-standalone.jar  # run the application with a specified port, timeout and retries
```

## REST API

### List all registered echo servers

- GET http://localhost:9000/server
- Content-Type: application/json

- Response
- Status-code: 200
- Body:
```json
{
  "servers": [
    {
      "url": "http://localhost:3000/echo"
    }
  ],
  "current": 1
}
```

### Register an echo server

- POST http://localhost:9000/server
- Content-Type: application/json
- Body:
```json
{"url": "http://localhost:3000/echo"}
```

- Response
- Status-code: 200
- Body:
```json
{"url": "http://localhost:3000/echo"}
```

### Remove an echo server

- DELETE http://localhost:9000/server
- Content-Type: application/json
- Body:
```json
{"url": "http://localhost:3000/echo"}
```

- Response
- Status-code: 202

### Post a request to be routed to the echo servers

- POST http://localhost:9000/route
- Content-Type: application/json
- Body
```json
{"game":"Mobile Legends", "gamerID":"GYUTDTE", "points":20}
```

- Response
- Status-code: 200
- Body:
```json
{
  "game": "Mobile Legends",
  "points": 20,
  "gamerID": "GYUTDTE",
  "timestamp": "2022-10-09T20:26:57.864Z",
  "host": "localhost:3000"
}
```

## Contact

Michael Angelo Calimlim `<macalimlim@gmail.com>`
