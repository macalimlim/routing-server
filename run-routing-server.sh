#!/bin/sh

lein clean && lein uberjar && ROUTING_SERVER_PORT="$1" ROUTING_SERVER_TIMEOUT="$2" ROUTING_SERVER_RETRIES="$3" java -jar /home/macalimlim/workspace/coda-payments/routing-server/target/routing-server-0.1.0-SNAPSHOT-standalone.jar
