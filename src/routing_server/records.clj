(ns routing-server.records
  (:require [clojure.core.async :refer [<!! >!! chan thread]]))

(defrecord Server [url])

(def servers-atom (atom []))

(def server-ch (chan))
(def add-remove-server-ch (chan))

(defn start-thread
  "starts a thread that continuously put server(s) into the server-channel in a round-robin manner"
  []
  (thread
    (loop [current 0]
      (let [servers      @servers-atom
            server       (get servers current)
            inc-or-id-fn (if server
                           (do (>!! server-ch server)
                               inc)
                           identity)]
        (recur (let [c (inc-or-id-fn current)]
                 (if (>= c (count servers)) 0 c)))))))

(defn round-robin-servers
  "takes a server from the server-channel"
  []
  (when (not-empty @servers-atom)
    (<!! server-ch)))

(defn- remove-from-servers
  [servers server]
  (into [] (remove (partial = server) servers)))

(defn- add-or-remove-server [id-or-not-fn conj-or-remove-fn server]
  (let [servers @servers-atom]
    (when (id-or-not-fn (some #(= server %) servers))
      (swap! servers-atom conj-or-remove-fn server))))

(defn- send-to-add-remove-server-ch
  [f s]
  (>!! add-remove-server-ch [f s]))

(defn add-server [server]
  (let [add-rem-fn (partial add-or-remove-server not conj)]
    (send-to-add-remove-server-ch add-rem-fn server)))

(defn remove-server [server]
  (let [add-rem-fn (partial add-or-remove-server identity remove-from-servers)]
    (send-to-add-remove-server-ch add-rem-fn server)))

(defn start-add-or-remove-thread
  []
  (thread
    (loop []
      (let [[add-rem-fn server] (<!! add-remove-server-ch)]
        (add-rem-fn server))
      (recur))))
