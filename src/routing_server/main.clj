(ns routing-server.main
  (:gen-class)
  (:require [duct.core :as duct]
            [routing-server.records :as rec])
  (:import [routing_server.records Server]))

(duct/load-hierarchy)

(defn -main [& args]
  ;; (rec/add-server (Server. "http://localhost:3000/echo"))
  ;; (rec/add-server (Server. "http://localhost:3001/echo"))
  ;; (rec/add-server (Server. "http://localhost:3002/echo"))
  (rec/start-thread)
  (rec/start-add-or-remove-thread)
  (let [keys     (or (duct/parse-keys args) [:duct/daemon])
        profiles [:duct.profile/prod]]
    (-> (duct/resource "routing_server/config.edn")
        (duct/read-config)
        (duct/exec-config profiles keys))
    (System/exit 0)))
