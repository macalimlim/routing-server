(ns routing-server.handler.register
  (:require [ataraxy.response :as response]
            [clojure.walk :as walk]
            [integrant.core :as ig]
            [routing-server.records :as rec])
  (:import [routing_server.records Server]))

(defmethod ig/init-key :routing-server.handler/register [_ _]
  (fn [{:keys [body-params]}]
    (let [server (Server. (:url (walk/keywordize-keys body-params)))]
      (rec/add-server server)
      {:status 200 :body body-params})))
