(ns routing-server.handler.list
 (:require [ataraxy.response :as response]
           [integrant.core :as ig]
           [routing-server.records :as rec]))

(defmethod ig/init-key :routing-server.handler/list [_ _]
  (fn [_]
    {:status 200 :body @rec/servers-atom}))
