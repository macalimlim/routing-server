(ns routing-server.handler.route
  (:require [clj-http.client :as http]
            [clojure.data.json :as json]
            [integrant.core :as ig]
            [routing-server.records :as rec]))

(defmethod ig/init-key :routing-server.handler/route [_ {:keys [timeout retries]}]
  (fn [{:keys [body-params content-type]}]
    (loop [num-retry 0]
      (cond
        (empty? @rec/servers-atom)  {:status 503} ;; service unavailable
        (> num-retry (dec retries)) {:status 502} ;; bad gateway
        :else
        (do
          (Thread/sleep (* num-retry 1000)) ;; back off (num-retry * 1000) ms
          (if-let [res (try
                         (http/post (:url (rec/round-robin-servers))
                                    {:body           (json/write-str body-params)
                                     :content-type   content-type
                                     :socket-timeout (* timeout 1000)})
                         (catch Exception _
                           nil))]
            res
            (recur (inc num-retry))))))))
