(ns routing-server.handler.remove
  (:require [ataraxy.response :as response]
            [clojure.walk :as walk]
            [integrant.core :as ig]
            [routing-server.records :as rec])
  (:import [routing_server.records Server]))

(defmethod ig/init-key :routing-server.handler/remove [_ _]
  (fn [{:keys [body-params]}]
    (let [server (Server. (:url (walk/keywordize-keys body-params)))]
      (rec/remove-server server)
      {:status 202})))
