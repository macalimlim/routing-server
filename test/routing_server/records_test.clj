(ns routing-server.records-test
  (:require [clojure.core.async :refer [chan]]
            [clojure.test :refer [deftest is testing]]
            [routing-server.records :as sut])
  (:import [routing_server.records Server]))

(deftest round-robin-servers
  (testing "1 server"
    (with-redefs [sut/servers-atom (atom [(Server. "server 1")])
                  sut/server-ch    (chan)]
      (sut/start-thread)
      (is (= (Server. "server 1") (sut/round-robin-servers)))))
  (testing "1 server 2x"
    (with-redefs [sut/servers-atom (atom [(Server. "server 1")])
                  sut/server-ch    (chan)]
      (sut/start-thread)
      (is (= (Server. "server 1") (sut/round-robin-servers)))
      (is (= (Server. "server 1") (sut/round-robin-servers)))))
  (testing "2 servers"
    (with-redefs [sut/servers-atom (atom [(Server. "server 1") (Server. "server 2")])
                  sut/server-ch    (chan)]
      (sut/start-thread)
      (is (= (Server. "server 1") (sut/round-robin-servers)))
      (is (= (Server. "server 2") (sut/round-robin-servers)))))
  (testing "2 servers 2x"
    (with-redefs [sut/servers-atom (atom [(Server. "server 1") (Server. "server 2")])
                  sut/server-ch    (chan)]
      (sut/start-thread)
      (is (= (Server. "server 1") (sut/round-robin-servers)))
      (is (= (Server. "server 2") (sut/round-robin-servers)))
      (is (= (Server. "server 1") (sut/round-robin-servers)))
      (is (= (Server. "server 2") (sut/round-robin-servers)))))
  (testing "3 servers"
    (with-redefs [sut/servers-atom (atom [(Server. "server 1") (Server. "server 2") (Server. "server 3")])
                  sut/server-ch    (chan)]
      (sut/start-thread)
      (is (= (Server. "server 1") (sut/round-robin-servers)))
      (is (= (Server. "server 2") (sut/round-robin-servers)))
      (is (= (Server. "server 3") (sut/round-robin-servers)))))
  (testing "3 servers 2x"
    (with-redefs [sut/servers-atom (atom [(Server. "server 1") (Server. "server 2") (Server. "server 3")])
                  sut/server-ch    (chan)]
      (sut/start-thread)
      (is (= (Server. "server 1") (sut/round-robin-servers)))
      (is (= (Server. "server 2") (sut/round-robin-servers)))
      (is (= (Server. "server 3") (sut/round-robin-servers)))
      (is (= (Server. "server 1") (sut/round-robin-servers)))
      (is (= (Server. "server 2") (sut/round-robin-servers)))
      (is (= (Server. "server 3") (sut/round-robin-servers)))))
  (testing "4 servers"
    (with-redefs [sut/servers-atom (atom [(Server. "server 1") (Server. "server 2") (Server. "server 3") (Server. "server 4")])
                  sut/server-ch    (chan)]
      (sut/start-thread)
      (let [ss (map (fn [_] (sut/round-robin-servers)) (range 4))]
        (is (= (nth ss 0) (Server. "server 1")))
        (is (= (nth ss 1) (Server. "server 2")))
        (is (= (nth ss 2) (Server. "server 3")))
        (is (= (nth ss 3) (Server. "server 4"))))))
  (testing "no servers"
    (with-redefs [sut/servers-atom (atom [])
                  sut/server-ch    (chan)]
      (sut/start-thread)
      (let [ss (map (fn [_] (sut/round-robin-servers)) (range 4))]
        (is (= (nth ss 0) nil))
        (is (= (nth ss 1) nil))
        (is (= (nth ss 2) nil))
        (is (= (nth ss 3) nil))))))

(deftest round-robin-servers-concurrent
  (testing "2 servers 3 requests"
    (with-redefs [sut/servers-atom (atom [(Server. "server 1") (Server. "server 2")])
                  sut/server-ch    (chan)]
      (sut/start-thread)
      (let [ss (pmap (fn [_] (sut/round-robin-servers)) (range 3))
            s1 (filter #(= (:url %) "server 1") ss)
            s2 (filter #(= (:url %) "server 2") ss)]
        (is (= (count s1) 2))
        (is (= (count s2) 1)))))
  (testing "3 servers 7 requests"
    (with-redefs [sut/servers-atom (atom [(Server. "server 1") (Server. "server 2") (Server. "server 3")])
                  sut/server-ch    (chan)]
      (sut/start-thread)
      (let [ss (pmap (fn [_] (sut/round-robin-servers)) (range 7))
            s1 (filter #(= (:url %) "server 1") ss)
            s2 (filter #(= (:url %) "server 2") ss)
            s3 (filter #(= (:url %) "server 3") ss)]
        (is (= (count s1) 3))
        (is (= (count s2) 2))
        (is (= (count s3) 2)))))
  (testing "4 servers 10 requests"
    (with-redefs [sut/servers-atom (atom [(Server. "server 1") (Server. "server 2") (Server. "server 3") (Server. "server 4")])
                  sut/server-ch    (chan)]
      (sut/start-thread)
      (let [ss (pmap (fn [_] (sut/round-robin-servers)) (range 10))
            s1 (filter #(= (:url %) "server 1") ss)
            s2 (filter #(= (:url %) "server 2") ss)
            s3 (filter #(= (:url %) "server 3") ss)
            s4 (filter #(= (:url %) "server 4") ss)]
        (is (= (count s1) 3))
        (is (= (count s2) 3))
        (is (= (count s3) 2))
        (is (= (count s4) 2))))))

(deftest add-server
  (testing "add 1 server"
    (with-redefs [sut/servers-atom         (atom [])
                  sut/server-ch            (chan)
                  sut/add-remove-server-ch (chan)]
      (sut/start-thread)
      (sut/start-add-or-remove-thread)
      (sut/add-server (Server. "server 1"))
      (Thread/sleep 100)
      (is (= (Server. "server 1") (sut/round-robin-servers)))
      (is (= 1 (count @sut/servers-atom)))))
  (testing "add 2 servers 2 requests"
    (with-redefs [sut/servers-atom         (atom [])
                  sut/server-ch            (chan)
                  sut/add-remove-server-ch (chan)]
      (sut/start-thread)
      (sut/start-add-or-remove-thread)
      (sut/add-server (Server. "server 1")) ;; at the point this server gets added the current goes back to 0
      (sut/add-server (Server. "server 2"))
      (Thread/sleep 100)
      (let [ss (pmap (fn [_] (sut/round-robin-servers)) (range 2))
            s1 (filter #(= (:url %) "server 1") ss)
            s2 (filter #(= (:url %) "server 2") ss)]
        (is (= (count s1) 2)) ;; this is natural, see comment above
        (is (= (count s2) 0)))))
  (testing "add 2 servers 3 requests"
    (with-redefs [sut/servers-atom         (atom [])
                  sut/server-ch            (chan)
                  sut/add-remove-server-ch (chan)]
      (sut/start-thread)
      (sut/start-add-or-remove-thread)
      (sut/add-server (Server. "server 1"))
      (sut/add-server (Server. "server 2"))
      (Thread/sleep 100)
      (let [ss (pmap (fn [_] (sut/round-robin-servers)) (range 3))
            s1 (filter #(= (:url %) "server 1") ss)
            s2 (filter #(= (:url %) "server 2") ss)]
        (is (= (count s1) 2))
        (is (= (count s2) 1)))))
  (testing "add 3 servers 4 requests"
    (with-redefs [sut/servers-atom         (atom [])
                  sut/server-ch            (chan)
                  sut/add-remove-server-ch (chan)]
      (sut/start-thread)
      (sut/start-add-or-remove-thread)
      (sut/add-server (Server. "server 1"))
      (sut/add-server (Server. "server 2"))
      (sut/add-server (Server. "server 3"))
      (Thread/sleep 100)
      (let [ss (pmap (fn [_] (sut/round-robin-servers)) (range 4))
            s1 (filter #(= (:url %) "server 1") ss)
            s2 (filter #(= (:url %) "server 2") ss)
            s3 (filter #(= (:url %) "server 3") ss)]
        (is (= (count s1) 2))
        (is (= (count s2) 1))
        (is (= (count s3) 1))))))

(deftest remove-server
  (testing "remove 1 server"
    (with-redefs [sut/servers-atom         (atom [(Server. "server 1")])
                  sut/server-ch            (chan)
                  sut/add-remove-server-ch (chan)]
      (sut/start-thread)
      (sut/start-add-or-remove-thread)
      (sut/remove-server (Server. "server 1"))
      (Thread/sleep 100)
      (is (= 0 (count @sut/servers-atom)))))
  (testing "remove 1 server from 2 servers"
    (with-redefs [sut/servers-atom         (atom [(Server. "server 1") (Server. "server 2")])
                  sut/server-ch            (chan)
                  sut/add-remove-server-ch (chan)]
      (sut/start-thread)
      (sut/start-add-or-remove-thread)
      (sut/remove-server (Server. "server 1"))
      (Thread/sleep 100)
      (let [ss (pmap (fn [_] (sut/round-robin-servers)) (range 6))
            s1 (filter #(= (:url %) "server 1") ss)
            s2 (filter #(= (:url %) "server 2") ss)]
        (is (= (count s1) 1))
        (is (= (count s2) 5))
        (is (= 1 (count @sut/servers-atom)))))))
