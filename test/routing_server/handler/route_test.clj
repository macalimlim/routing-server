(ns routing-server.handler.route-test
  (:require [clj-http.client :as http]
            [clojure.core.async :refer [chan]]
            [clojure.test :refer [deftest is testing]]
            [integrant.core :as ig]
            [ring.mock.request :as mock]
            [routing-server.handler.route :as route]
            [routing-server.records :as rec])
  (:import [routing_server.records Server]))

(deftest route-endpoint-test
  (testing "route endpoint exists"
    (with-redefs [rec/servers-atom (atom [(Server. "server 1")])
                  rec/server-ch    (chan)
                  http/post        (fn [url req]
                                     (-> req
                                         (assoc :body url)
                                         (assoc :status 200)))]
      (rec/start-thread)
      (let [handler  (ig/init-key :routing-server.handler/route {:timeout 5 :retries 10})
            response (handler (mock/request :post "/route"))]
        (is (= (:status response) 200)))))
  (testing "route endpoint 3 servers 2x"
    (with-redefs [rec/servers-atom (atom [(Server. "server 1") (Server. "server 2") (Server. "server 3")])
                  rec/server-ch    (chan)
                  http/post        (fn [url req]
                                     (-> req
                                         (assoc :body url)
                                         (assoc :status 200)))]
      (rec/start-thread)
      (let [handler (ig/init-key :routing-server.handler/route {:timeout 5 :retries 10})
            res1    (handler (mock/request :post "/route"))
            res2    (handler (mock/request :post "/route"))
            res3    (handler (mock/request :post "/route"))
            res4    (handler (mock/request :post "/route"))
            res5    (handler (mock/request :post "/route"))
            res6    (handler (mock/request :post "/route"))]
        (is (= (:body res1) "server 1"))
        (is (= (:body res2) "server 2"))
        (is (= (:body res3) "server 3"))
        (is (= (:body res4) "server 1"))
        (is (= (:body res5) "server 2"))
        (is (= (:body res6) "server 3")))))
  (testing "route endpoint 3 servers 10 requests"
    (with-redefs [rec/servers-atom (atom [(Server. "server 1") (Server. "server 2") (Server. "server 3")])
                  rec/server-ch    (chan)
                  http/post        (fn [url req]
                                     (-> req
                                         (assoc :body url)
                                         (assoc :status 200)))]
      (rec/start-thread)
      (let [handler (ig/init-key :routing-server.handler/route {:timeout 5 :retries 10})
            res1    (handler (mock/request :post "/route"))
            res2    (handler (mock/request :post "/route"))
            res3    (handler (mock/request :post "/route"))
            res4    (handler (mock/request :post "/route"))
            res5    (handler (mock/request :post "/route"))
            res6    (handler (mock/request :post "/route"))
            res7    (handler (mock/request :post "/route"))
            res8    (handler (mock/request :post "/route"))
            res9    (handler (mock/request :post "/route"))
            res10   (handler (mock/request :post "/route"))]
        (is (= (:body res1) "server 1"))
        (is (= (:body res2) "server 2"))
        (is (= (:body res3) "server 3"))
        (is (= (:body res4) "server 1"))
        (is (= (:body res5) "server 2"))
        (is (= (:body res6) "server 3"))
        (is (= (:body res7) "server 1"))
        (is (= (:body res8) "server 2"))
        (is (= (:body res9) "server 3"))
        (is (= (:body res10) "server 1")))))
  (testing "route endpoint 0 servers 5 requests"
    (with-redefs [rec/servers-atom (atom [])
                  rec/server-ch    (chan)
                  http/post        (fn [url req]
                                     (-> req
                                         (assoc :body url)
                                         (assoc :status 200)))]
      (rec/start-thread)
      (let [handler (ig/init-key :routing-server.handler/route {:timeout 5 :retries 10})
            res1    (handler (mock/request :post "/route"))
            res2    (handler (mock/request :post "/route"))
            res3    (handler (mock/request :post "/route"))
            res4    (handler (mock/request :post "/route"))
            res5    (handler (mock/request :post "/route"))]
        (is (= (:status res1) 503))
        (is (= (:status res2) 503))
        (is (= (:status res3) 503))
        (is (= (:status res4) 503))
        (is (= (:status res5) 503)))))
  (testing "route endpoint 1 server is down 5 requests" ;; this test takes longer because of the back off
    (with-redefs [rec/servers-atom (atom [(Server. "invalid url")])
                  rec/server-ch    (chan)
                  http/post        (fn [_ _]
                                     nil)]
      (rec/start-thread)
      (let [handler (ig/init-key :routing-server.handler/route {:timeout 5 :retries 10})
            res1    (handler (mock/request :post "/route"))
            res2    (handler (mock/request :post "/route"))
            res3    (handler (mock/request :post "/route"))
            res4    (handler (mock/request :post "/route"))
            res5    (handler (mock/request :post "/route"))]
        (is (= (:status res1) 502))
        (is (= (:status res2) 502))
        (is (= (:status res3) 502))
        (is (= (:status res4) 502))
        (is (= (:status res5) 502))))))

(deftest route-endpoint-test-concurrent
  (testing "route endpoint 3 servers 2x"
    (with-redefs [rec/servers-atom (atom [(Server. "server 1") (Server. "server 2") (Server. "server 3")])
                  rec/server-ch    (chan)
                  http/post        (fn [url req]
                                     (-> req
                                         (assoc :body url)
                                         (assoc :status 200)))]
      (rec/start-thread)
      (let [handler (ig/init-key :routing-server.handler/route {:timeout 5 :retries 10})
            rss     (pmap (fn [_] (handler (mock/request :post "/route"))) (range 6))
            s1      (filter #(= (:body %) "server 1") rss)
            s2      (filter #(= (:body %) "server 2") rss)
            s3      (filter #(= (:body %) "server 3") rss)]
        (is (= (count s1) 2))
        (is (= (count s2) 2))
        (is (= (count s3) 2)))))
  (testing "route endpoint 3 servers 10 requests"
    (with-redefs [rec/servers-atom (atom [(Server. "server 1") (Server. "server 2") (Server. "server 3")])
                  rec/server-ch    (chan)
                  http/post        (fn [url req]
                                     (-> req
                                         (assoc :body url)
                                         (assoc :status 200)))]
      (rec/start-thread)
      (let [handler (ig/init-key :routing-server.handler/route {:timeout 5 :retries 10})
            rss     (pmap (fn [_] (handler (mock/request :post "/route"))) (range 10))
            s1      (filter #(= (:body %) "server 1") rss)
            s2      (filter #(= (:body %) "server 2") rss)
            s3      (filter #(= (:body %) "server 3") rss)]
        (is (= (count s1) 4))
        (is (= (count s2) 3))
        (is (= (count s3) 3)))))
  (testing "route endpoint 0 servers 5 requests"
    (with-redefs [rec/servers-atom (atom [])
                  rec/server-ch    (chan)
                  http/post        (fn [url req]
                                     (-> req
                                         (assoc :body url)
                                         (assoc :status 200)))]
      (rec/start-thread)
      (let [handler (ig/init-key :routing-server.handler/route {:timeout 5 :retries 10})
            rss     (pmap (fn [_] (handler (mock/request :post "/route"))) (range 5))
            r503    (filter #(= (:status %) 503) rss)]
        (is (= (count r503) 5)))))
  (testing "route endpoint 1 server is down 5 requests" ;; this test takes longer because of the back off
    (with-redefs [rec/servers-atom (atom [(Server. "invalid url")])
                  rec/server-ch    (chan)
                  http/post        (fn [_ _]
                                     nil)]
      (rec/start-thread)
      (let [handler (ig/init-key :routing-server.handler/route {:timeout 5 :retries 10})
            rss     (pmap (fn [_] (handler (mock/request :post "/route"))) (range 5))
            r502    (filter #(= (:status %) 502) rss)]
        (is (= (count r502) 5)))))
  (testing "route endpoint 2 servers 1 server is up and 1 server is down 5 requests"
    (with-redefs [rec/servers-atom (atom [(Server. "server 1") (Server. "down")])
                  rec/server-ch    (chan)
                  http/post        (fn [url req]
                                     (if (= url "down")
                                       nil
                                       (-> req
                                           (assoc :body url)
                                           (assoc :status 200))))]
      (rec/start-thread)
      (let [handler (ig/init-key :routing-server.handler/route {:timeout 5 :retries 10})
            rss     (pmap (fn [_] (handler (mock/request :post "/route"))) (range 5))
            rs      (filter #(= (:status %) 200) rss)]
        (is (= (count rs) 5)))))
  (testing "route endpoint 2 servers 1 server is up and 1 server has timeout 5 requests" ;; same behavior when its down
    (with-redefs [rec/servers-atom (atom [(Server. "server 1") (Server. "timeout")])
                  rec/server-ch    (chan)
                  http/post        (fn [url req]
                                     (if (= url "timeout")
                                       nil
                                       (-> req
                                           (assoc :body url)
                                           (assoc :status 200))))]
      (rec/start-thread)
      (let [handler (ig/init-key :routing-server.handler/route {:timeout 5 :retries 10})
            rss     (pmap (fn [_] (handler (mock/request :post "/route"))) (range 5))
            rs      (filter #(= (:status %) 200) rss)]
        (is (= (count rs) 5)))))
  (testing "route endpoint 3 servers 2 servers up and 1 server is down 10 requests"
    (with-redefs [rec/servers-atom (atom [(Server. "server 1") (Server. "down") (Server. "server 3")])
                  rec/server-ch    (chan)
                  http/post        (fn [url req]
                                     (if (= url "down")
                                       nil
                                       (-> req
                                           (assoc :body url)
                                           (assoc :status 200))))]
      (rec/start-thread)
      (let [handler (ig/init-key :routing-server.handler/route {:timeout 5 :retries 10})
            rss     (pmap (fn [_] (handler (mock/request :post "/route"))) (range 10))
            s1      (filter #(= (:body %) "server 1") rss)
            s3      (filter #(= (:body %) "server 3") rss)]
        (is (= (count s1) 5))
        (is (= (count s3) 5)))))
  (testing "route endpoint 3 servers 2 servers up and 1 server has timeout 10 requests" ;; same behavior when its down
    (with-redefs [rec/servers-atom (atom [(Server. "server 1") (Server. "timeout") (Server. "server 3")])
                  rec/server-ch    (chan)
                  http/post        (fn [url req]
                                     (if (= url "timeout")
                                       nil
                                       (-> req
                                           (assoc :body url)
                                           (assoc :status 200))))]
      (rec/start-thread)
      (let [handler (ig/init-key :routing-server.handler/route {:timeout 5 :retries 10})
            rss     (pmap (fn [_] (handler (mock/request :post "/route"))) (range 10))
            s1      (filter #(= (:body %) "server 1") rss)
            s3      (filter #(= (:body %) "server 3") rss)]
        (is (= (count s1) 5))
        (is (= (count s3) 5))))))
